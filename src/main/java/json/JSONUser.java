package json;

import jsonfilling.Flower;

import java.io.File;
import java.util.List;

public class JSONUser {
    public static void main(String[] args) {
        File json = new File("src/main/resources/orangeryJSON.json");
        File scheme = new File("src/main/resources/orangeryJSONscheme.json");

        JSONParser parser = new JSONParser();
        printList(parser.getFlowerList(json));
    }
    private static void printList(List<Flower> fl) {
        System.out.println("JSON");
        for (Flower flower : fl) {
            System.out.println(flower);
        }
    }
}
