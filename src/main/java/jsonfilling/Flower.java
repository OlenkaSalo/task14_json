package jsonfilling;

import java.util.ArrayList;
import java.util.List;

public class Flower {
    private String name;
    private String soil;
    private String origin;
    private String multiplying;
    private List<VisualParameters> VisualParameters = new ArrayList<VisualParameters>();
    private GrowingTips growingTips;
    public Flower(){}
    public Flower(String name, String soil, String origin, String multiplying, List<VisualParameters> VisualParameters , GrowingTips growingTips)
    {
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.multiplying = multiplying;
        this.VisualParameters = VisualParameters;
        this.growingTips = growingTips;
    }
    public void setName(String name){ this.name=name;}
    public void setSoil(String soil){this.soil=soil;}
    public void setOrigin(String origin){this.origin=origin;}
    public void setMultiplying(String multiplying){this.multiplying=multiplying;}
    public void setVisualParameters(List<VisualParameters> VisualParameters){this.VisualParameters= VisualParameters;}
    public void setGrowingTips(GrowingTips growingTips){this.growingTips=growingTips;}

    public String getName(){return name;}
    public String getSoil(){return soil;}
    public String getOrigin(){return origin;}
    public String getMultiplying(){return multiplying;}
    public List<VisualParameters> getVisualParameters(){return VisualParameters;}
    public GrowingTips getGrowingTips(){return growingTips;}

    @Override
    public String toString()
    {
        return   "Flower{" +
                " name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin=" + origin +
                ", multiplying='" + multiplying + '\'' +
                ", " + VisualParameters +
                ", " + growingTips+
                '}';
    }
}
