package jsonfilling;

public class GrowingTips {
    private double temperature;
    private boolean luminosity;
    private int watering;

    public GrowingTips(){}
    public GrowingTips(double temperature, boolean luminosity, int watering)
    {
        this.temperature = temperature;
        this.luminosity = luminosity;
        this.watering = watering;
    }

    public void setTemperature(double temperature){this.temperature=temperature; }
    public void setLuminosity(boolean luminosity){this.luminosity=luminosity;}
    public void setWatering(int watering){this.watering=watering;}

    public double getTemperature(){return temperature;}
    public boolean getLuminosity(){return luminosity;}
    public int getWatering(){return watering;}

    @Override
    public String toString(){
        return "GrowingTips{" + "temperature - > " + temperature +
                " luminosity - > " + luminosity +
                " watering - > " +watering +
                '}';
    }
}
