package jsonfilling;

import java.util.List;

public class Community {
    private List<Flower> flowerList;

    public void setFlowerList(List<Flower> flowerList){
        this.flowerList=flowerList;
    }

    public List<Flower> getFlower()
    {
        return flowerList;
    }

}
