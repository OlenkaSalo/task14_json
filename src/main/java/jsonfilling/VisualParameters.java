package jsonfilling;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VisualParameters {
    private String parameter;

    public VisualParameters(){}

    public VisualParameters(String parameter){this.parameter=parameter;}

    public void setParameter(String parameter){this.parameter=parameter;}
    public String getParameter(){return parameter;}

    @Override
    public String toString()
    {
        return "VisualParameters{" + " parameter ->'" + parameter + '\'' + '}';
    }
}
